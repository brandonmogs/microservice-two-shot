# Wardrobify

Team:

* Angel Lawler Skye - Shoes
* Brandon Moghadam - Hats

# Instructions To Run

* clone our repository.
* from the root directory, run these commands, one after another:
 "docker volume create two-shot-pgdata"
 "docker-compose build"
 "docker-compose up"

 * Go to localhost:3000 to see our React Front-End.


## Design

## Shoes microservice

I created a Shoe model containing the fields "manufacturer", "model_name", "color", "picture_url", and "bin". I created the BinVO model to hold the data that records which bin each shoe is stored in. The field of "bin" in my Shoe model is a foreign key to my BinVO model. My BinVO model contains data that is collected by polling for new instances of the Bin model from the Wardrobe microservice at specfied intervals so my BinVO database stays synced with the Bins that exist in the Wardrobe microservice.

## Hats microservice

i made a model for a hat that includes properties , i also created a locationvo to be able to properly sort my hats within my database in an organized manner. i created views that handle posting new hats and deleting existing hats on the webpage to allow the user to seemlessly do what they want. my hat form consits of the properties listed in my hat model with the option to place it in a specific locationvo.
