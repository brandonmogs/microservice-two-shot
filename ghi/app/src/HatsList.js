function HatsList(props) {
    const handleDelete = async (id) => {
        try {
            const response = await fetch(`http://localhost:8090/api/hats/${id}`, {
                method: 'DELETE',
            });

            if (response.ok) {
                console.log(`Hat with ID ${id} deleted successfully`);
            } else {
                console.error('Error deleting hat');
            }
        } catch (error) {
            console.error('Error deleting hat:', error);
        }
    };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Color</th>
                    <th>Fabric</th>
                    <th>Location</th>
                    <th>Picture</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{ hat.name }</td>
                            <td>{ hat.color }</td>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.location.closet_name }</td>
                            <td>
                                <img src={ hat.picture } alt={hat.name} style={{ maxWidth: '100px' }} />
                            </td>
                            <td>
                                <button onClick={() => handleDelete(hat.id)}>Delete</button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}

export default HatsList;
