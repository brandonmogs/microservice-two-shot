import React, { useState, useEffect } from 'react';

const ShoeForm = () => {
  const [manufacturer, setManufacturer] = useState('');
  const [model_name, setModelName] = useState('');
  const [color, setColor] = useState('');
  const [picture_url, setPictureUrl] = useState('');
  const [closet, setCloset] = useState('');
  const [bin, setBin] = useState('');
  const [bins, setBins] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('http://localhost:8100/api/bins/');
        if (!response.ok) {
          throw new Error(`Network response was not ok (Status: ${response.status})`);
        }
        const data = await response.json();
        if (!data.bins) {
          throw new Error('Bins data not available');
        }
        setBins(data.bins);
      } catch (error) {
        console.error('Error fetching bins:', error);
      }
    };

    fetchData();
  }, []);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    switch (name) {
      case 'manufacturer':
        setManufacturer(value);
        break;
      case 'model_name':
        setModelName(value);
        break;
      case 'color':
        setColor(value);
        break;
      case 'picture_url':
        setPictureUrl(value);
        break;
      case 'closet':
        setCloset(value);
        break;
      case 'bin':
        setBin(value);
        break;
      default:
        break;
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const response = await fetch('http://localhost:8080/api/shoes/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          manufacturer,
          model_name,
          color,
          picture_url,
          bin,
        }),
      });

      if (!response.ok) {
        throw new Error(`Network response was not ok (Status: ${response.status})`);
      }

      setManufacturer('');
      setModelName('');
      setColor('');
      setPictureUrl('');
      setCloset('');
      setBin('');
    } catch (error) {
      console.error('Error creating shoe:', error);
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h2>Add a New Pair Of Shoes</h2>
          <form onSubmit={handleSubmit}>
            <div className="form-group">
              <label htmlFor="manufacturer">Manufacturer:</label>
              <input type="text" className="form-control" id="manufacturer" name="manufacturer" value={manufacturer} onChange={handleInputChange} />
            </div>
            <div className="form-group">
              <label htmlFor="model_name">Model Name:</label>
              <input type="text" className="form-control" id="model_name" name="model_name" value={model_name} onChange={handleInputChange} />
            </div>
            <div className="form-group">
              <label htmlFor="color">Color:</label>
              <input type="text" className="form-control" id="color" name="color" value={color} onChange={handleInputChange} />
            </div>
            <div className="form-group">
              <label htmlFor="picture_url">Picture URL:</label>
              <input type="text" className="form-control" id="picture_url" name="picture_url" value={picture_url} onChange={handleInputChange} />
            </div>
            <div className="form-group">
              <label htmlFor="closet">Closet Name:</label>
              <select className="form-control" id="closet" name="closet" value={closet} onChange={handleInputChange}>
                <option value="">select</option>
                {[...new Set(bins.map((bin) => bin.closet_name))].map((uniqueClosetName) => (
                  <option key={uniqueClosetName} value={uniqueClosetName}>
                    {uniqueClosetName}
                  </option>
                ))}
              </select>
            </div>
            <div className="form-group">
              <label htmlFor="bin">Bin Number:</label>
              <select className="form-control" name="bin" value={bin} onChange={handleInputChange}>
                <option value="">select</option>
                {bins
                  .filter((bin) => bin.closet_name === closet)
                  .map((bin) => (
                    <option key={bin.id} value={bin.bin_number}>
                      {bin.bin_number}
                    </option>
                  ))}
              </select>
            </div>
            <button type="submit" className="btn btn-primary">Add a New Shoe</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ShoeForm;