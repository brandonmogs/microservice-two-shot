import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList'
import ShoeForm from './ShoeForm'
import { useState, useEffect } from 'react';
import HatsList from './HatsList.js';
import HatForm from './HatForm.js';

function App() {
  const [hats,setHats] = useState([])

  async function loadHats() {
    const response = await fetch('http://localhost:8090/api/hats/');
    const {hats} = await response.json();
    setHats(hats)
  }

  useEffect(() => {
    loadHats();
  }, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoeList />} />
          <Route path="/shoes/new" element={<ShoeForm />} />
          <Route path="hats">
          <Route path ="list" element={<HatsList hats={hats} />} />
          <Route path="create" element={<HatForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
