import React, { useState, useEffect } from 'react';

function HatForm({ getHats }) {
    const [name, setName] = useState('')
    const [color, setColor] = useState('')
    const [fabric, setFabric] = useState('')
    const [location, setLocation] = useState('')
    const [picture, setPicture] = useState('')
    const [locations, setLocations] = useState([]);

    useEffect(() => {
        async function fetchLocations() {
            try {
                const response = await fetch('http://localhost:8100/api/locations')
                const data = await response.json()
                setLocations(data.locations)
            } catch (error) {
            }
        }

        fetchLocations();
    }, []);

    async function handleSubmit(event) {
        event.preventDefault()
        const data = {
            name,
            color,
            fabric,
            location,
            picture,
        }

        const HatURL = 'http://localhost:8090/api/hats/'
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(HatURL, fetchOptions)
        if (response.ok) {
            const newhat = await response.json();
            console.log(newhat);
            setName('');
            setColor('');
            setFabric('');
            setLocation('');
            setPicture('');
        } else {
            const errorData = await response.json();
            console.error('Error:', errorData);
        }
    }

    function handleChangeLocation(event) {
        const { value } = event.target;
        setLocation(value);
    }

    function handleChangeName(event) {
        const { value } = event.target;
        setName(value);
    }

    function handleChangeColor(event) {
        const { value } = event.target;
        setColor(value);
    }

    function handleChangeFabric(event) {
        const { value } = event.target;
        setFabric(value);
    }

    function handleChangePicture(event) {
        const { value } = event.target;
        setPicture(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input value={fabric} onChange={handleChangeFabric} placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={name} onChange={handleChangeName} placeholder="style" required type="text" name="style" id="style" className="form-control" />
                            <label htmlFor="starts">Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleChangeColor} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={picture} onChange={handleChangePicture} placeholder="picture" required type="url" name="picture" id="picture" className="form-control" />
                            <label htmlFor="picture">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select value={location} onChange={handleChangeLocation} required name="location" id="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.closet_name} value={location.href}>{location.closet_name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create Hat</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default HatForm;
