import React, { useState, useEffect } from 'react';

const ShoeList = () => {
  // Use useState to manage component state
  const [shoes, setShoes] = useState([]);
  const [loading, setLoading] = useState(true);
  const [deleteConfirmation, setDeleteConfirmation] = useState(null);

  // Use useEffect to replace componentDidMount
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('http://localhost:8080/api/shoes/');
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        const data = await response.json();
        setShoes(data.shoes);
        setLoading(false);
      } catch (error) {
        console.error('Error fetching shoes:', error);
        setLoading(false);
      }
    };

    fetchData();
  }, []); // An empty dependency array is equivalent to componentDidMount

  // Function to handle delete confirmation
  const handleDelete = (id) => {
    setDeleteConfirmation(id);
  };

  // Function to confirm and delete a shoe
  const confirmDelete = async () => {
    if (deleteConfirmation) {
      try {
        const response = await fetch(`http://localhost:8080/api/shoes/${deleteConfirmation}`, {
          method: 'DELETE',
        });

        if (!response.ok) {
          throw new Error('Network response was not ok');
        }

        // Update the shoes state by filtering out the deleted shoe
        setShoes((prevShoes) => prevShoes.filter((shoe) => shoe.id !== deleteConfirmation));
        setDeleteConfirmation(null); // Clear the confirmation dialog
      } catch (error) {
        console.error('Error deleting shoe:', error);
      }
    }
  };

  // Function to cancel the delete operation
  const cancelDelete = () => {
    setDeleteConfirmation(null);
  };

  return (
    <div>
      {loading ? (
        <p>Loading...</p>
      ) : (
        <ul>
          {shoes.map((shoe) => (
            <li key={shoe.id}>
              <h2>{shoe.model_name}</h2>
              <img
                src={shoe.picture_url}
                alt="Shoe.img"
                style={{ width: '250px', height: '250px' }}
              />
              <div style={{ marginBottom: '10px' }}></div>

              {/* Dropdown to show all shoe details */}
              <details>
                <div style={{ marginBottom: '10px' }}></div>
                <summary>Show Details</summary>
                <div style={{ marginBottom: '10px' }}></div>
                <p>Manufacturer: {shoe.manufacturer}</p>
                <p>Color: {shoe.color}</p>
                <p>
                  Current Location: {shoe.bin.closet_name}, Bin: {shoe.bin.bin_number}
                </p>
              </details>
              <div style={{ marginBottom: '10px' }}></div>

              {/* Button to delete the shoe */}
              <button onClick={() => handleDelete(shoe.id)}>Delete</button>
              <div style={{ marginBottom: '30px' }}></div>

              {deleteConfirmation === shoe.id && (
                <div>
                  <p>Are you sure you want to delete this shoe?</p>
                  <button onClick={confirmDelete}>Yes</button>
                  <button onClick={cancelDelete}>No</button>
                </div>
              )}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default ShoeList;
