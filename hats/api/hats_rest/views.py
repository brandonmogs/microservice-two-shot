from .models import Hat, LocationVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]


class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "name",
        "fabric",
        "color",
        "picture",
        "location",
    ]

    encoders = {"location": LocationVODetailEncoder()}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "color",
        "fabric",
        "location",
        "picture",
    ]
    encoders = {"location": LocationVODetailEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_id=None):
    if request.method == "GET":
        if location_id is not None:
            hats = Hat.objects.filter(location=location_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_hat_detail(request, pk):
    try:
        hat = Hat.objects.get(pk=pk)
    except Hat.DoesNotExist:
        return JsonResponse({"message": "Hat not found."}, status=404)

    if request.method == "GET":
        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)
    elif request.method == "PUT":
        pass
    elif request.method == "DELETE":
        hat.delete()
        return JsonResponse({"message": "Hat deleted successfully."}, status=200)
