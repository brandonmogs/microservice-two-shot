from django.urls import path
from .views import api_list_hats, api_hat_detail


urlpatterns = [
    path("hats/", api_list_hats, name="hat-list"),
    path("hats/<int:pk>/", api_hat_detail, name="hat-detail"),
]
